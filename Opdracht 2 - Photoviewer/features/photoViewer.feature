Feature: Photo Viewer

Scenario: Open Photo Viewer
    When I open a site with the url "file:///C:/Users/Ernie/Dropbox/HAN-prive/CRIA/WT/Opdracht%202%20-%20Photoviewer/index.html"
    # And I maximize the browser window
    Then I expect the title to be "Photo Viewer"

Scenario: The pop up
    When I press the spacebar
    Then I expect the pop up to show

    When I press the "right arrow" key
    Then I expect the next image to show
    When I press the "left arrow" key
    Then I expect the previous image to show

    When I click on the pop up
    Then I expect the pop up to be closed

Scenario: Right click function
    When I right click on an image in the overview
    Then I expect the total number of images in the overview to be 24
    When I right click on an image in the overview
    Then I expect the total number of images in the overview to be 23
    
