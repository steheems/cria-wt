/*global require:true, module:true */
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    assert = require('chai').assert,
    expect = require('chai').expect;

var lastOpenedImage, currentlyOpenedImage;

var sleepTime = 0;

var photoViewerStepDefinition = function () {
    var driver = new webdriver.Builder().forBrowser('firefox').build();

    //////////
    // When //
    //////////

    this.When(/^I maximize the browser window$/, function (next) {
        driver.manage().window().maximize().then(next);
    });

    this.When(/^I open a site with the url "([^"]*)"$/, function (url, next) {
        driver.get(url).then(next);
    });

    this.When(/^I press the spacebar$/, function (next) {
        driver.findElement(By.css('body')).sendKeys(webdriver.Key.SPACE).then(next);
    });

    this.When(/^I press the "([^"]*)" key$/, function (keyString, next) {
        var keyToSend;

        switch (keyString) {
        case 'up arrow':
            keyToSend = webdriver.Key.ARROW_UP;
            break;
        case 'down arrow':
            keyToSend = webdriver.Key.ARROW_DOWN;
            break;
        case 'left arrow':
            keyToSend = webdriver.Key.ARROW_LEFT;
            break;
        case 'right arrow':
            keyToSend = webdriver.Key.ARROW_RIGHT;
            break;
        default:
            keyToSend = keyString;
            break;
        }
        lastOpenedImage = currentlyOpenedImage;
        driver.findElement(By.css('body')).sendKeys(keyToSend).then(function () {
            currentlyOpenedImage = driver.findElement(By.css('div#enlarged-photo > img'));
            next();
        });
    });

    this.When(/^I click on the pop up$/, function (next) {
        driver.findElement(By.css('div#enlarged-photo')).click().then(function () {
            next();
        });
    });

    this.When(/^I right click on an image in the overview$/, function (next) {
        var image = driver.findElement(By.css('div#photo-viewer > img'));

        new webdriver.ActionSequence(driver).click(image, webdriver.Button.RIGHT).perform().then(function () {
            next();
        });
    });

    //////////
    // Then //
    //////////

    this.Then(/^I expect the title to be "([^"]*)"$/, function (title, next) {
        driver.getTitle().then(function (actualTitle) {
            expect(actualTitle).to.equal(title);
            next();
        });
    });

    this.Then(/^I expect the pop up to show$/, function (next) {
        driver.findElement(By.css('div#photo-viewer-background')).then(function (elem) {
            expect(elem).to.not.equal(null);

            driver.sleep(sleepTime);
            next();
        });
    });

    this.Then(/^I expect the pop up to be closed$/, function (next) {
        driver.findElement(By.css('div#photo-viewer-background')).then(function (elem) {
            expect(elem).to.equal(null);
            driver.sleep(sleepTime);
            next();
        }, function (err) {
            expect(err.name).to.equal('NoSuchElementError');
            driver.sleep(sleepTime);
            next();
        });
    });

    this.Then(/^I expect the (?:next|previous) image to show$/, function (next) {
        expect(currentlyOpenedImage).to.not.equal(lastOpenedImage);
        driver.sleep(sleepTime);
        next();
    });

    this.Then(/^I expect the total number of images in the overview to be (\d+)$/, function (numberOfImages, next) {
        driver.findElements(By.css('div#photo-viewer > img')).then(function (elements) {
            expect(elements.length).to.equal(Number(numberOfImages));
            driver.sleep(sleepTime);
            next();
        });
    });
};

module.exports = photoViewerStepDefinition;