/*jslint browser: true, plusplus: true */

/**
 * Main PhotoViewer class
 * Create a new instance to activate the PhotoViewer on your website
 *
 * @param {JSON string} picturesJson A JSON containing the location of the images to be used within the PhotoViewer
 * Example of a pictures JSON:
 * {"pictures":[
 *     {"description":"This is an image", "url":"images/image.jpg"},
 *     {"description":"This is an image", "url":"http://www.test.test/images/image.jpg"}
 * ]}
 */
var PhotoViewer = function (picturesJson) {
    'use strict';

    var localThis, photoViewerElement;

    localThis = this;

    photoViewerElement = document.createElement('div');
    photoViewerElement.setAttribute('id', 'photo-viewer');
    document.body.appendChild(photoViewerElement);

    localThis.picturesObject = JSON.parse(picturesJson);

    photoViewerElement.setAttribute('class', 'row-5');

    localThis.populatePhotoViewerOverview();

    document.addEventListener('keydown', function (event) {
        //Press escape key
        if (event.keyCode === 27 && localThis.photoViewerIsOpen) {
            event.preventDefault();
            localThis.closePhotoViewer();
        }

        //Press spacebar
        if (event.keyCode === 32) {
            event.preventDefault();
            localThis.pressSpacebar();
        }

        //Press left arrow key
        if (event.keyCode === 37 && localThis.photoViewerIsOpen) {
            event.preventDefault();
            localThis.openPreviousImage();
        }

        //Press uparrow key
        if (event.keyCode === 38 && localThis.photoViewerIsOpen) {
            event.preventDefault();
            localThis.openImageAbove();
        }

        //Press right arrow key
        if (event.keyCode === 39 && localThis.photoViewerIsOpen) {
            event.preventDefault();
            localThis.openNextImage();
        }

        //Press down arrow key
        if (event.keyCode === 40 && localThis.photoViewerIsOpen) {
            event.preventDefault();
            localThis.openImageBelow();
        }
    });

    document.addEventListener('click', function (event) {
        //Click on an image in the PhotoViewer overview
        if (event.target.tagName === 'IMG' && event.target.parentNode.getAttribute('id') === 'photo-viewer') {
            event.preventDefault();
            localThis.openPopup(event.target);
        }

        //Click on the PhotoViewer pop-up
        if (event.target.parentNode.getAttribute('id') === 'photo-viewer-background' ||
                event.target.parentNode.parentNode.getAttribute('id') === 'photo-viewer-background') {
            event.preventDefault();
            localThis.closePhotoViewer();
        }
    });

    photoViewerElement.addEventListener('mouseup', function (event) {
        //Right click removes image
        if (event.button === 2 && event.target.tagName === 'IMG') {
            event.preventDefault();
            localThis.removeImageElement(event.target);
        }
    });

    photoViewerElement.addEventListener('contextmenu', function (event) {
        //Prevent the right click context menu from showing up
        if (event.target.tagName === 'IMG') {
            event.preventDefault();
        }
    });

    photoViewerElement.addEventListener('dragstart', function (event) {
        if (event.target.tagName === 'IMG') {
            localThis.drag(event);
        }
    });

    photoViewerElement.addEventListener('dragover', function (event) {
        if (event.target.tagName === 'IMG') {
            localThis.allowDrop(event);
        }
    });

    photoViewerElement.addEventListener('drop', function (event) {
        if (event.target.tagName === 'IMG') {
            localThis.drop(event);
        }
    });
};

/**
 * Create the prototype
 */
PhotoViewer.prototype = Object.create(PhotoViewer.prototype);

/**
 * Is the Photo Viewer popup open or not
 */
PhotoViewer.prototype.photoViewerIsOpen = false;
/**
 * The image currently opened in the popup
 */
PhotoViewer.prototype.currentlyOpenedImage = null;
/**
 * Object containing all the pictures, parsed from a JSON file/string
 */
PhotoViewer.prototype.picturesObject = null;
/**
 * When using drag and drop, this is the element being dragged
 */
PhotoViewer.prototype.dragSource = null;

/**
 * Create the 'img' elements within the PhotoViewer overview
 * 
 */
PhotoViewer.prototype.populatePhotoViewerOverview = function () {
    'use strict';

    var x, photoViewerElement;

    photoViewerElement = document.getElementById('photo-viewer');

    for (x = 0; x < this.picturesObject.pictures.length; x++) {
        photoViewerElement.appendChild(this.createImageElement('class', this.randomSkewedClass(), this.picturesObject.pictures[x].url, this.picturesObject.pictures[x].description));
    }
};

/**
 * Generate a random class of either 'skewed-left', 'skewed-right', or an empty string
 * 
 * @return {string} The random name of a class
 */
PhotoViewer.prototype.randomSkewedClass = function () {
    'use strict';

    var classes, randNo;

    classes = ['', 'skewed-left', 'skewed-right'];
    randNo = Math.floor(Math.random() * 3);

    return classes[randNo];
};

/**
 * Opens the next image to the one currently opened
 */
PhotoViewer.prototype.openNextImage = function () {
    'use strict';

    if (this.currentlyOpenedImage.nextSibling !== null && this.currentlyOpenedImage.nextSibling.tagName === 'IMG') {
        this.openPopup(this.currentlyOpenedImage.nextSibling);
    } else {
        this.openPopup(this.getFirstImage());
    }
};

/**
 * Opens the previous image to the one currently opened
 */
PhotoViewer.prototype.openPreviousImage = function () {
    'use strict';

    if (this.currentlyOpenedImage.previousSibling !== null && this.currentlyOpenedImage.previousSibling.tagName === 'IMG') {
        this.openPopup(this.currentlyOpenedImage.previousSibling);
    } else {
        this.openPopup(this.getLastImage());
    }
};

/**
 * Opens the image above the one currently opened
 * Wraps around the top and starts again at the bottom
 */
PhotoViewer.prototype.openImageAbove = function () {
    'use strict';

    var counter, counterDifference, functionImage = this.currentlyOpenedImage;

    functionImage = this.currentlyOpenedImage;

    try {
        for (counter = 0; counter < this.getNumberOfImagesPerRow(); counter++) {
            functionImage = functionImage.previousSibling;
            if (functionImage !== null && functionImage.tagName !== 'IMG') {
                counter--;
            }
        }
    } catch (ignore) {
        //do nothing
        //necessary for wraping around the top
    }

    if (functionImage === null) {
        functionImage = this.getLastImage();

        counterDifference = this.getNumberOfImagesPerRow() - counter;

        for (counter = 0; counter < counterDifference; counter++) {
            functionImage = functionImage.previousSibling;
            if (functionImage !== null && functionImage.tagName !== 'IMG') {
                counter--;
            }
        }
    }

    this.openPopup(functionImage);
};

/**
 * Opens the image below the one currently opened
 * Wraps around the bottom and starts again at the top
 */
PhotoViewer.prototype.openImageBelow = function () {
    'use strict';

    var counter, counterDifference, functionImage;

    functionImage = this.currentlyOpenedImage;

    try {
        for (counter = 0; counter < this.getNumberOfImagesPerRow(); counter++) {
            functionImage = functionImage.nextSibling;
            if (functionImage !== null && functionImage.tagName !== 'IMG') {
                counter--;
            }
        }
    } catch (ignore) {
        //do nothing
        //necessary for wraping around the bottom
    }

    if (functionImage === null) {
        functionImage = this.getFirstImage();

        counterDifference = this.getNumberOfImagesPerRow() - counter;

        for (counter = 0; counter < counterDifference; counter++) {
            functionImage = functionImage.nextSibling;
            if (functionImage !== null && functionImage.tagName !== 'IMG') {
                counter--;
            }
        }
    }

    this.openPopup(functionImage);
};

/**
 * Create an 'img' element in the PhotoViewer
 *  
 * @param  {string} classOrId  Either the string 'class' or 'id'
 * @param  {string} identifier The name of the class or string to be used for the 'img' element
 * @param  {string} src        The 'src' attribute to be used for the 'img' element (location of the image)
 * @param  {string} alt        The 'alt' attribute to be used for the 'img' element (text to be shown when image is not found)
 * @return {DOM element}       An 'img' element inside
 */
PhotoViewer.prototype.createImageElement = function (classOrId, identifier, src, alt) {
    'use strict';

    var imageElement = document.createElement('img');
    imageElement.setAttribute('src', src);
    imageElement.setAttribute('alt', alt);
    imageElement.setAttribute('draggable', 'true');
    imageElement.setAttribute(classOrId, identifier);

    return imageElement;
};

/**
 * Remove an 'img' element from the PhotoViewer
 * 
 * @param  {DOM element} elem The image element to remove
 */
PhotoViewer.prototype.removeImageElement = function (elem) {
    'use strict';

    //if the image that is to be removed is the last opened image, clear that variable
    if (this.currentlyOpenedImage === elem) {
        this.currentlyOpenedImage = null;
    }

    elem.parentNode.removeChild(elem);
    this.changeImageSizeInRow();

    //repopulate the overview when the last image is removed
    if (this.getNumberOfImages() === 0) {
        this.populatePhotoViewerOverview();
    }
};

/**
 * Create the caption to an image element
 * 
 * @param  {DOM 'img' element} elem The image element that needs a caption
 * @return {DOM 'div' element}      The caption div
 */
PhotoViewer.prototype.createImageCaption = function (elem) {
    'use strict';

    var x, imageObject, captionDiv, authorSpan, dateSpan, br;

    captionDiv = document.createElement('div');

    for (x = 0; x < this.picturesObject.pictures.length; x++) {
        if (this.picturesObject.pictures[x].url === elem.getAttribute('src')) {
            imageObject = this.picturesObject.pictures[x];
        }
    }

    authorSpan = document.createElement('span');
    authorSpan.textContent += 'Author: ' + imageObject.author;

    br = document.createElement('br');

    dateSpan = document.createElement('span');
    dateSpan.textContent += 'Date: ' + imageObject.date;

    captionDiv.appendChild(authorSpan);
    captionDiv.appendChild(br);
    captionDiv.appendChild(dateSpan);
    captionDiv.setAttribute('id', 'enlarged-photo-caption');

    return captionDiv;
};

/**
 * Opens the actual PhotoViewer pop-up.
 * 
 * @param  {DOM element} elem The 'a' element which contains the img to show
 */
PhotoViewer.prototype.openPopup = function (elem) {
    'use strict';

    var offsetY, background, backgroundColor, existingBackground, enlargedPhotoDiv, newEnlargedPhotoDiv;

    offsetY = window.pageYOffset;

    this.preventScroll();

    this.currentlyOpenedImage = elem;

    if (!this.photoViewerIsOpen) {
        //PhotoViewer popup is not yet open

        background = document.createElement('div');

        background.setAttribute('id', 'photo-viewer-background');
        background.style.top = offsetY + 'px';

        backgroundColor = document.createElement('div');
        backgroundColor.setAttribute('id', 'photo-viewer-background-color');

        document.body.insertBefore(background, document.body.firstChild);

        background.appendChild(backgroundColor);

        enlargedPhotoDiv = document.createElement('div');
        enlargedPhotoDiv.setAttribute('id', 'enlarged-photo');

        enlargedPhotoDiv.appendChild(this.createImageElement('id', 'enlarged-photo-img', elem.getAttribute('src'), elem.getAttribute('alt')));

        enlargedPhotoDiv.appendChild(this.createImageCaption(elem));

        background.appendChild(enlargedPhotoDiv);

        this.photoViewerIsOpen = true;
    } else {
        //PhotoViewer popup is already open

        existingBackground = document.getElementById('photo-viewer-background');

        enlargedPhotoDiv = document.getElementById('enlarged-photo');
        enlargedPhotoDiv.parentNode.removeChild(enlargedPhotoDiv);

        newEnlargedPhotoDiv = document.createElement('div');
        newEnlargedPhotoDiv.setAttribute('id', 'enlarged-photo');

        newEnlargedPhotoDiv.appendChild(this.createImageElement('id', 'enlarged-photo-img', elem.getAttribute('src'), elem.getAttribute('alt')));

        newEnlargedPhotoDiv.appendChild(this.createImageCaption(elem));

        existingBackground.appendChild(newEnlargedPhotoDiv);
    }
};

/**
 * Close the currently open PhotoViewer
 * 
 */
PhotoViewer.prototype.closePhotoViewer = function () {
    'use strict';

    var existingBackground = document.getElementById('photo-viewer-background');

    this.allowScroll();

    document.body.removeChild(existingBackground);
    this.photoViewerIsOpen = false;
};

PhotoViewer.prototype.preventDefault = function (event) {
    'use strict';

    event.preventDefault();
};

/**
 * Prevents the html body from scrolling
 * 
 */
PhotoViewer.prototype.preventScroll = function () {
    'use strict';

    window.onmousewheel = document.onmousewheel = this.preventDefault;
};

/**
 * Allows the html body to scroll
 * 
 */
PhotoViewer.prototype.allowScroll = function () {
    'use strict';

    window.onmousewheel = document.onmousewheel = null;
};

/**
 * Returns the first image in the PhotoViewer overview
 * 
 * @return {[DOM element} First 'img' element found in the 'photo-viewer' div
 */
PhotoViewer.prototype.getFirstImage = function () {
    'use strict';

    return document.getElementById('photo-viewer').getElementsByTagName('img')[0];
};

/**
 * Returns the last image in the PhotoViewer overview
 * 
 * @return {[DOM element} Last 'img' element found in the 'photo-viewer' div
 */
PhotoViewer.prototype.getLastImage = function () {
    'use strict';

    var imgElements = document.getElementById('photo-viewer').getElementsByTagName('img');
    return imgElements[imgElements.length - 1];
};

/**
 * What to do when the spacebar is pressed
 * 
 */
PhotoViewer.prototype.pressSpacebar = function () {
    'use strict';

    var lastOpenedImg;

    if (!this.photoViewerIsOpen) {
        lastOpenedImg = this.currentlyOpenedImage;
        if (this.currentlyOpenedImage === null) {
            lastOpenedImg = this.getFirstImage();
        }

        this.openPopup(lastOpenedImg);
    } else {
        this.closePhotoViewer();
    }
};

/**
 * Changes the class of PhotoViewer to adjust how many images are displayed per row
 * 
 */
PhotoViewer.prototype.changeImageSizeInRow = function () {
    'use strict';

    var rowClass;

    if (this.getNumberOfImagesPerRow() > 0) {
        rowClass = 'row-' + this.getNumberOfImagesPerRow();
    } else {
        rowClass = 'row-5';
    }

    document.getElementById('photo-viewer').setAttribute('class', rowClass);
};

/**
 * Get the maximum number of images currently displayed in one row
 * @return {[type]} [description]
 */
PhotoViewer.prototype.getNumberOfImagesPerRow = function () {
    'use strict';

    return this.calculateNumOfRowImages(this.getNumberOfImages());
};

/**
 * Get the number of images currently displayed in the PhotoViewer overview
 * 
 * @return {Number} Number of images in the PhotoViewer
 */
PhotoViewer.prototype.getNumberOfImages = function () {
    'use strict';

    return document.getElementById('photo-viewer').getElementsByTagName('img').length;
};

/**
 * Function to calculate number of images on a row
 *
 * @return {number} The number of images which should be on one row
 */
PhotoViewer.prototype.calculateNumOfRowImages = function (numOfImages) {
    'use strict';

    return Math.ceil(Math.sqrt(numOfImages));
};

/**
 * Handles the start of a drag event
 * @param  {DOM event} event The actual drag event
 */
PhotoViewer.prototype.drag = function (event) {
    'use strict';

    // event.dataTransfer.effectAllowed = 'move';
    event.dataTransfer.setData('text/html', event.target.outerHTML);
    this.dragSource = event.target;
};

/**
 * Allows a dragged item to be dropped
 * @param  {DOM event} event The dragover event
 */
PhotoViewer.prototype.allowDrop = function (event) {
    'use strict';

    event.preventDefault();
    // event.dataTransfer.dropEffect = 'move';
};

/**
 * Handles dropping an item
 * @param  {DOM event} event The actual drop event
 */
PhotoViewer.prototype.drop = function (event) {
    'use strict';

    event.preventDefault();
    var data = event.dataTransfer.getData('text/html');

    if (this.dragSource !== event.target) {
        this.dragSource.outerHTML = event.target.outerHTML;
        event.target.outerHTML = data;
    }
};