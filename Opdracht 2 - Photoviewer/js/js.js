/*jslint browser: true, plusplus: true */
/*global PhotoViewer: true */

// A JSON string with the image locations
var picturesJson = '{"pictures":[' +
    '{"description":"This is an image", "url":"images/0004681.jpg", "author":"A. Testerson", "date":"05-04-2014"},' +
    '{"description":"This is an image", "url":"images/0004713.jpg", "author":"B. Testerson", "date":"01-03-2014"},' +
    '{"description":"This is an image", "url":"images/0004720.jpg", "author":"C. Testerson", "date":"10-12-2014"},' +
    '{"description":"This is an image", "url":"images/0004731.jpg", "author":"D. Testerson", "date":"08-10-2014"},' +
    '{"description":"This is an image", "url":"images/0004750.jpg", "author":"E. Testerson", "date":"17-10-2014"},' +
    '{"description":"This is an image", "url":"images/0004755.jpg", "author":"F. Testerson", "date":"30-10-2014"},' +
    '{"description":"This is an image", "url":"images/0004801.jpg", "author":"G. Testerson", "date":"17-02-2014"},' +
    '{"description":"This is an image", "url":"images/0004802.jpg", "author":"H. Testerson", "date":"11-03-2014"},' +
    '{"description":"This is an image", "url":"images/0004827.jpg", "author":"I. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004853.jpg", "author":"J. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004858.jpg", "author":"K. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004860.jpg", "author":"L. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004870.jpg", "author":"M. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004874.jpg", "author":"N. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004888.jpg", "author":"O. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004902.jpg", "author":"P. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004931.jpg", "author":"Q. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004969.jpg", "author":"R. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0004971.jpg", "author":"S. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0006598.jpg", "author":"T. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0006630.jpg", "author":"U. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0006638.jpg", "author":"V. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0006680.jpg", "author":"W. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0006743.jpg", "author":"X. Testerson", "date":"17-09-2014"},' +
    '{"description":"This is an image", "url":"images/0006787.jpg", "author":"Y. Testerson", "date":"17-09-2014"}' +
    ']}';

//Wait for the entire DOM to be loaded
document.addEventListener('DOMContentLoaded', function () {
    'use strict';

    var photoViewer = new PhotoViewer(picturesJson);
});
