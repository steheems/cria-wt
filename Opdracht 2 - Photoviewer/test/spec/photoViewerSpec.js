describe("PhotoViewer", function() {
  var photoViewer;

  // A JSON string with the image locations
  var picturesJson = '{"pictures":[' +
    '{"description":"This is an image", "url":"images/0004681.jpg", "author":"A. Testerson", "date":"05-04-2014"},' +
    '{"description":"This is an image", "url":"images/0004713.jpg", "author":"B. Testerson", "date":"01-03-2014"},' +
    '{"description":"This is an image", "url":"images/0006787.jpg", "author":"Y. Testerson", "date":"17-09-2014"}' +
    ']}';

  beforeEach(function() {
    spyOn(PhotoViewer.prototype, 'populatePhotoViewerOverview').and.callThrough();
    spyOn(PhotoViewer.prototype, 'randomSkewedClass').and.callThrough();
    spyOn(PhotoViewer.prototype, 'openNextImage').and.callThrough();
    spyOn(PhotoViewer.prototype, 'openPreviousImage').and.callThrough();
    spyOn(PhotoViewer.prototype, 'openImageAbove').and.callThrough();
    spyOn(PhotoViewer.prototype, 'openImageBelow').and.callThrough();
    spyOn(PhotoViewer.prototype, 'createImageElement').and.callThrough();
    spyOn(PhotoViewer.prototype, 'removeImageElement').and.callThrough();
    spyOn(PhotoViewer.prototype, 'createImageCaption').and.callThrough();
    spyOn(PhotoViewer.prototype, 'openPopup').and.callThrough();
    spyOn(PhotoViewer.prototype, 'closePhotoViewer').and.callThrough();
    spyOn(PhotoViewer.prototype, 'preventDefault').and.callThrough();
    spyOn(PhotoViewer.prototype, 'preventScroll').and.callThrough();
    spyOn(PhotoViewer.prototype, 'allowScroll').and.callThrough();
    spyOn(PhotoViewer.prototype, 'getFirstImage').and.callThrough();
    spyOn(PhotoViewer.prototype, 'getLastImage').and.callThrough();
    spyOn(PhotoViewer.prototype, 'pressSpacebar').and.callThrough();
    spyOn(PhotoViewer.prototype, 'changeImageSizeInRow').and.callThrough();
    spyOn(PhotoViewer.prototype, 'getNumberOfImagesPerRow').and.callThrough();
    spyOn(PhotoViewer.prototype, 'getNumberOfImages').and.callThrough();
    spyOn(PhotoViewer.prototype, 'calculateNumOfRowImages').and.callThrough();
    spyOn(PhotoViewer.prototype, 'drag').and.callThrough();
    spyOn(PhotoViewer.prototype, 'allowDrop').and.callThrough();
    spyOn(PhotoViewer.prototype, 'drop').and.callThrough();

    spyOn(document, 'addEventListener').and.callThrough();
    spyOn(Element.prototype, 'addEventListener').and.callThrough();

    spyOn(document, 'createElement').and.callThrough();
  });

  it("should create a div with img elements in it", function() {
    photoViewer = new PhotoViewer(picturesJson);

    expect(photoViewer.picturesObject).toEqual(JSON.parse(picturesJson));

    expect(PhotoViewer.prototype.populatePhotoViewerOverview).toHaveBeenCalled();
    expect(PhotoViewer.prototype.createImageElement).toHaveBeenCalled();
    expect(PhotoViewer.prototype.randomSkewedClass).toHaveBeenCalled();
    expect(document.addEventListener).toHaveBeenCalledWith('click', jasmine.any(Function));
    expect(Element.prototype.addEventListener).toHaveBeenCalledWith('mouseup', jasmine.any(Function));
    expect(Element.prototype.addEventListener).toHaveBeenCalledWith('contextmenu', jasmine.any(Function));
    expect(Element.prototype.addEventListener).toHaveBeenCalledWith('dragstart', jasmine.any(Function));
    expect(Element.prototype.addEventListener).toHaveBeenCalledWith('dragover', jasmine.any(Function));
    expect(Element.prototype.addEventListener).toHaveBeenCalledWith('drop', jasmine.any(Function));
  });

  it("should do something with the spacebar", function() {
    photoViewer = new PhotoViewer(picturesJson);

    expect(photoViewer.picturesObject).toEqual(JSON.parse(picturesJson));

    expect(photoViewer.photoViewerIsOpen).toEqual(false);

    photoViewer.pressSpacebar();
    
    expect(photoViewer.photoViewerIsOpen).toEqual(true);
    expect(photoViewer.currentlyOpenedImage).not.toEqual(null);
    expect(PhotoViewer.prototype.openPopup).toHaveBeenCalled();
    expect(PhotoViewer.prototype.preventScroll).toHaveBeenCalled();
    expect(document.createElement).toHaveBeenCalledWith('div');
    expect(PhotoViewer.prototype.createImageElement).toHaveBeenCalled();

    photoViewer.pressSpacebar();

    expect(photoViewer.photoViewerIsOpen).toEqual(false);
    expect(PhotoViewer.prototype.closePhotoViewer).toHaveBeenCalled();
    expect(PhotoViewer.prototype.allowScroll).toHaveBeenCalled();
  });

});
