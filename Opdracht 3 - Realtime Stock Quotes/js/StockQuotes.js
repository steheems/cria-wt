/*jslint browser: true, plusplus: true */
/*global console: true, YahooApiHelper: true, $: true */


/**
 * StockQuotes Drawer class
 * Draws the table and rows
 */
var SqDrawer = function (yahooHelper) {
    'use strict';

    this.values = [];

    /**
     * Draw a table in the document body
     * 
     * @param  {Array} quotes Array of quote objects returned from the Yahoo API
     */
    this.drawTable = function (quotes) {
        var sqDiv, sqTable, x, sqDivId, sqDivFound;

        sqDivId = 'stock-quotes';

        sqDivFound = document.getElementById(sqDivId);

        if (sqDivFound !== null) {
            sqDivFound.parentNode.removeChild(sqDivFound);
        }

        sqDiv = document.createElement('div');
        sqDiv.id = sqDivId;
        document.body.appendChild(sqDiv);

        sqTable = document.createElement('table');

        this.drawTableHeader(sqTable, yahooHelper.getSelectFields());

        for (x = 0; x < quotes.length; x++) {
            this.drawRow(sqTable, quotes[x]);
        }

        sqDiv.appendChild(sqTable);

        this.drawInCanvases(quotes);
        this.activateSparkline(quotes);
    };

    /**
     * Draw the header at the top of the table
     * 
     * @param  {Table} parentTable          The table in which the header needs to be drawn
     * @param  {Array of strings} arrayOfHeaderContent The content of the header
     */
    this.drawTableHeader = function (parentTable, arrayOfHeaderContent) {
        var sqHeaderRow, sqTh, x;

        sqHeaderRow = document.createElement('tr');

        for (x = 0; x < arrayOfHeaderContent.length; x++) {
            sqTh = document.createElement('th');
            sqTh.textContent = arrayOfHeaderContent[x];
            sqHeaderRow.appendChild(sqTh);
        }

        parentTable.appendChild(sqHeaderRow);
    };

    /**
     * Draws a single row in the table
     * 
     * @param  {Table} parentTable The table this row needs to be drawn in
     * @param  {Object} quoteObject The object with the quote information
     */
    this.drawRow = function (parentTable, quoteObject) {
        var x, sqRow, changeNumber, currentField, time, d;

        sqRow = document.createElement('tr');

        for (x = 0; x < yahooHelper.getSelectFields().length; x++) {
            currentField = yahooHelper.getSelectFields()[x];
            if (currentField === 'LastTradeTime') {
                time = quoteObject[currentField];
                d = Date.parseExact(time, 'h:mmtt').setTimezone('EST');
                sqRow.appendChild(this.drawCell(d.toString('HH:mm')));
            } else {
                sqRow.appendChild(this.drawCell(quoteObject[currentField]));
            }
        }

        changeNumber = Number(quoteObject.Change);

        if (changeNumber < 0) {
            sqRow.className = 'down';
        } else if (changeNumber > 0) {
            sqRow.className = 'up';
        } else {
            sqRow.className = 'same';
        }

        sqRow.appendChild(this.createCanvasTd(quoteObject.Symbol));

        if (this.values[quoteObject.Symbol] === undefined) {
            this.values[quoteObject.Symbol] = [];
        }

        if (this.values[quoteObject.Symbol].length === 40) {
            this.values[quoteObject.Symbol].shift();
        }

        this.values[quoteObject.Symbol].push(quoteObject.Change);

        sqRow.appendChild(this.drawSparklineTd(quoteObject.Symbol));

        parentTable.appendChild(sqRow);
    };

    /**
     * Generate a single cell/td for use in a row
     * 
     * @param  {String} content The content to be used within the cell/td
     * @return {TD}         A TD element
     */
    this.drawCell = function (content) {
        var td = document.createElement('td');
        td.textContent = content;
        return td;
    };

    /**
     * Generate a special TD for use with the Sparkline plugin
     * 
     * @param  {String} symbol The symbol to be used
     * @return {TD}        A TD element
     */
    this.drawSparklineTd = function (symbol) {
        var sparklineId = 'sparkline-' + symbol.replace('.', ''),
            td = document.createElement('td');
        td.id = sparklineId;

        return td;
    };

    /**
     * Generate a special TD for use with canvas
     * 
     * @param  {String} symbol The symbol to be used
     * @return {TD}        A TD element
     */
    this.createCanvasTd = function (symbol) {
        var td = document.createElement('td'),
            canvas = document.createElement('canvas');

        canvas.id = 'canvas-' + symbol.replace('.', '');
        canvas.setAttribute('width', '20');
        canvas.setAttribute('height', '20');

        td.appendChild(canvas);

        return td;
    };

    /**
     * Using the canvas TDs created with the createCanvasTD function
     * draw arrows dependent on the quote's change
     * 
     * @param  {Array} quotes Array of quote objects
     */
    this.drawInCanvases = function (quotes) {
        var canvas, canvasContext, x, quote, changeNumber;

        for (x = 0; x < quotes.length; x++) {
            quote = quotes[x];
            canvas = document.getElementById('canvas-' + quote.Symbol.replace('.', ''));
            canvasContext = canvas.getContext('2d');

            changeNumber = Number(quote.Change);

            if (changeNumber < 0) {
                this.drawCanvasArrowDown(canvasContext);
            } else if (changeNumber > 0) {
                this.drawCanvasArrowUp(canvasContext);
            } else {
                this.drawCanvasLine(canvasContext);
            }
        }
    };

    /**
     * Draw an arrow pointing up
     * 
     * @param  {Canvas Context} canvasContext The context of the canvas to draw on
     */
    this.drawCanvasArrowUp = function (canvasContext) {
        canvasContext.moveTo(10, 20);
        canvasContext.lineTo(10, 0);
        canvasContext.moveTo(0, 10);
        canvasContext.lineTo(10, 0);
        canvasContext.moveTo(20, 10);
        canvasContext.lineTo(10, 0);

        canvasContext.strokeStyle = '#000';
        canvasContext.stroke();
    };

    /**
     * Draw an arrow pointing down
     * 
     * @param  {Canvas Context} canvasContext The context of the canvas to draw on
     */
    this.drawCanvasArrowDown = function (canvasContext) {
        canvasContext.moveTo(10, 0);
        canvasContext.lineTo(10, 20);
        canvasContext.moveTo(0, 10);
        canvasContext.lineTo(10, 20);
        canvasContext.moveTo(20, 10);
        canvasContext.lineTo(10, 20);

        canvasContext.strokeStyle = '#000';
        canvasContext.stroke();
    };

    /**
     * Draw a small line
     * 
     * @param  {Canvas Context} canvasContext The context of the canvas to draw on
     */
    this.drawCanvasLine = function (canvasContext) {
        canvasContext.moveTo(5, 10);
        canvasContext.lineTo(10, 10);

        canvasContext.strokeStyle = '#000';
        canvasContext.stroke();
    };

    /**
     * Activates the Sparkline plugin in every TD created with the drawSparklineTd function
     * 
     * @param  {Array} quotes Array of quote objects
     */
    this.activateSparkline = function (quotes) {
        var sparklineId, x;

        for (x = 0; x < quotes.length; x++) {
            sparklineId = 'sparkline-' + quotes[x].Symbol.replace('.', '');
            $('#' + sparklineId).sparkline(this.values[quotes[x].Symbol], {type: 'bar', barColor: 'black'});
        }
    };
};

/**
 * SqRequestCreator
 * Creates the XMLHttpRequest for use with Stock Quotes
 */
var SqRequestCreator = function () {
    'use strict';

    this.yahooHelper.addSelectFields(['Symbol', 'Name', 'Ask', 'Bid', 'Change', 'PercentChange', 'LastTradeTime', 'StockExchange']);
    this.yahooHelper.addSymbols(['AAPL', 'YHOO', 'GOOG', 'MSFT', 'ASUUY', 'TWTR', 'FB', 'LGEN.L', 'KN.PA', 'NTRS', 'BCS', 'GLE.PA', 'SDR.L', 'SL.L', 'BNP.PA', 'SNE', 'NTDOF', 'NTDOY', 'AFRAF', 'RYDAF', 'MCD', 'K']);
};

/**
 * How often a request is made
 */
SqRequestCreator.prototype.refreshRate = 1000;

/**
 * Instance of the YahooApiHelper
 * Used to generate an URL for use in the request
 */
SqRequestCreator.prototype.yahooHelper = new YahooApiHelper();

/**
 * Instance of the SqDrawer
 * Used to draw the table
 */
SqRequestCreator.prototype.drawer = new SqDrawer(SqRequestCreator.prototype.yahooHelper);

/**
 * Request Response activated when the XmlHttpRequest is returned
 * Keeps calling createRequest() indefinitely
 */
SqRequestCreator.prototype.requestResponse = function () {
    'use strict';

    var DONE = this.DONE || 4,
        responseObject;

    if (this.readyState === DONE) {
        setTimeout(function () {
            SqRequestCreator.prototype.createRequest();
        }, SqRequestCreator.prototype.refreshRate);

        if (this.status === 200) {
            responseObject = JSON.parse(this.response);
            if (Array.isArray(responseObject.query.results.quote)) {
                SqRequestCreator.prototype.drawer.drawTable(responseObject.query.results.quote);
            } else {
                SqRequestCreator.prototype.drawer.drawTable(SqRequestCreator.prototype.yahooHelper.generateRandomData());
            }
        } else {
            SqRequestCreator.prototype.drawer.drawTable(SqRequestCreator.prototype.yahooHelper.generateRandomData());
        }
    }
};

/**
 * Function perform when the XmlHttpRequest times out
 */
SqRequestCreator.prototype.requestResponseNoInternet = function () {
    'use strict';

    SqRequestCreator.prototype.drawer.drawTable(SqRequestCreator.prototype.yahooHelper.generateRandomData());
};


/**
 * Create a XmlHttpRequest
 */
SqRequestCreator.prototype.createRequest = function () {
    'use strict';

    var request = new XMLHttpRequest();
    request.onreadystatechange = this.requestResponse;
    // request.timeout = 4000;
    request.ontimeout = this.requestResponseNoInternet;

    request.open('GET', this.yahooHelper.getUrl(), true);
    request.send();
};

/**
 * Main StockQuotes class
 */
var StockQuotes = function () {
    'use strict';

    /**
     * Start the first request
     * Doesn't end
     * 
     * @param  {Number} refreshRate How often a new request is created in miliseconds
     */
    this.start = function (refreshRate) {
        if (refreshRate) {
            this['__proto__']['__proto__'].refreshRate = refreshRate;
        }
        this.createRequest();
    };
};

StockQuotes.prototype = new SqRequestCreator();
StockQuotes.prototype.constructor = StockQuotes;