/*jslint plusplus: true */
/*global escape: true */

/**
 * YahooApiHelper class
 * Helps to create the URL for getting the quotes
 */
var YahooApiHelper = function () {
    'use strict';

    var symbols, selectFields, quotify, pushStuff, randNumber, getEscapedString;

    /**
     * The symbols to be requested.
     *
     * Example:
     * ['AAPL', 'MSFT', 'YHOO']
     * 
     * @type {Array}
     */
    symbols = [];

    /**
     * The fields to be requested.
     *
     * Example:
     * ['Symbol', 'Change', 'LastTradeTime']
     * 
     * @type {Array}
     */
    selectFields = [];

    /**
     * Put quotes around the string
     * 
     * @param  {String} inputString The string that needs quotes around it
     * @return {String}             The string with quotes around it
     */
    quotify = function (inputString) {
        var localString = inputString;

        if (localString.charAt(0) === '"') {
            localString = localString.substr(1);
        }

        if (localString.charAt(localString.length - 1) === '"') {
            localString = localString.substr(0, localString.length - 1);
        }

        localString = '"' + localString + '"';

        return localString;
    };

    /**
     * Puts stuff inside an array
     * 
     * @param  {String or array of strings} thingToPush   The thing(s) to push into an array
     * @param  {Array} arrayToPushTo The array to push to
     * @param  {Boolean} doQuotify     Whether or not to put quotes around the strings before pushing
     */
    pushStuff = function (thingToPush, arrayToPushTo, doQuotify) {
        var x, localThingToPush;

        localThingToPush = thingToPush;

        if (typeof localThingToPush === 'string') {
            if (doQuotify) {
                localThingToPush = quotify(localThingToPush);
            }
            arrayToPushTo.push(localThingToPush);
        } else if (Array.isArray(localThingToPush)) {
            for (x = 0; x < localThingToPush.length; x++) {
                if (doQuotify) {
                    localThingToPush[x] = quotify(localThingToPush[x]);
                }
                arrayToPushTo.push(localThingToPush[x]);
            }
        }
    };

    /**
     * Generate a random number
     * 
     * @param  {Number} maxNumber   The maximum number to be included
     * @param  {Boolean} allowFloat Whether or not to allow float numbers
     * @return {[type]}             A random number from 0 until the maximum number given
     */
    randNumber = function (maxNumber, allowFloat) {
        if (allowFloat) {
            return (Math.floor(Math.random() * (Number(maxNumber) + 1) * 100)) / 100;
        }

        return Math.floor(Math.random() * (Number(maxNumber) + 1));
    };

    /**
     * Replaces special characters for use within the generated url
     * @param  {String} stringToEscape The string which needs to be escaped
     * @return {String}                The escaped string
     */
    getEscapedString = function (stringToEscape) {
        var returnString = stringToEscape;
        returnString = escape(returnString);
        return returnString;
    };

    /**
     * Add a symbol to request from the Yahoo Finance database
     * 
     * @param {String / Array of strings} symbol A symbol or array of symbols to add to this helper
     */
    this.addSymbol = this.addSymbols = function (symbol) {
        pushStuff(symbol, symbols, true);
    };

    /**
     * Returns the symbols currently selected in this helper
     * 
     * @return {Array of strings} Array of currently selected symbols
     */
    this.getSymbols = function () {
        return symbols;
    };

    /**
     * Add a field to be selected from the Yahoo Finance database
     * 
     * @param {String / Array of strings} field The field or array of fields to be selected
     */
    this.addSelectField = this.addSelectFields = function (field) {
        pushStuff(field, selectFields);
    };

    /**
     * Returns the selected fields currently selected in this helper
     * 
     * @return {Array of strings} Currently selected fields
     */
    this.getSelectFields = function () {
        return selectFields;
    };

    /**
     * Returns the URL to be used to request the data from the Yahoo API
     * 
     * @return {String} The URL that returns the stock quotes from Yahoo Finance
     */
    this.getUrl = function () {
        var url = 'https://query.yahooapis.com/v1/public/yql?q=select%20' + getEscapedString(selectFields.join(',')) + '%20from%20yahoo.finance.quotes%20where%20symbol%20in%0A(' + getEscapedString(symbols.join(',')) + ')&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=';
        return url;
    };

    /**
     * Function to generate random data when there is no internet
     * 
     * @return {Array} Array of symbol objects which is usually returned by the Yahoo API
     */
    this.generateRandomData = function () {
        var x, y, changeNumber, tempSym, returnArray = [], d = new Date();

        d.addHours(-6);

        for (x = 0; x < symbols.length; x++) {
            tempSym = {};
            for (y = 0; y < selectFields.length; y++) {
                switch (selectFields[y]) {
                case 'Symbol':
                    tempSym[selectFields[y]] = symbols[x].substring(1, symbols[x].length - 1);
                    break;
                case 'Change':
                    changeNumber = randNumber(5, true);

                    if (randNumber(1) === 0) {
                        changeNumber = changeNumber * -1;
                    }

                    tempSym[selectFields[y]] = changeNumber;
                    break;
                case 'LastTradeTime':
                    tempSym[selectFields[y]] = d.toString('h:mmtt');
                    break;
                default:
                    tempSym[selectFields[y]] = 'temp';
                    break;
                }
            }

            returnArray.push(tempSym);
        }

        return returnArray;
    };
};