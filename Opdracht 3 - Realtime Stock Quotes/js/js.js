/*jslint browser: true */
/*global StockQuotes: true */
document.addEventListener('DOMContentLoaded', function () {
    'use strict';
    var sq = new StockQuotes();

    sq.start(1000);
});