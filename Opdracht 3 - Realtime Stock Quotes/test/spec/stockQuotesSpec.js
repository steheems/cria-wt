/*jslint browser: true */
/*global describe: true, beforeEach: true, it: true, expect: true, StockQuotes: true, SqRequestCreator: true, SqDrawer: true, YahooApiHelper: true*/
describe("StockQuotes", function () {
    'use strict';

    var stockQuotes, sqRequestCreator, sqDrawer, helper, testTable;

    beforeEach(function () {
        helper = new YahooApiHelper();

        helper.addSelectFields(['Symbol', 'Change']);
        helper.addSymbol('TEST');

        stockQuotes = new StockQuotes();
        sqRequestCreator = new SqRequestCreator();
        sqDrawer = new SqDrawer(helper);

        testTable = document.createElement('table');
    });

    it("should draw a table header", function () {
        var foundElements;

        sqDrawer.drawTableHeader(testTable, ['Test', 'Another']);

        foundElements = testTable.getElementsByTagName('th');

        expect(foundElements.length).toEqual(2);
    });

    it("should draw a table row", function () {
        var foundElements;

        sqDrawer.drawRow(testTable, {Symbol: 'TEST', Change: 2.50});

        foundElements = testTable.getElementsByTagName('td');

        expect(foundElements.length).toEqual(4);

        expect(foundElements[0].textContent).toEqual('TEST');
        expect(foundElements[1].textContent).toEqual('2.5');
        expect(foundElements[2].getElementsByTagName('canvas')[0].getAttribute('id')).toEqual('canvas-TEST');
        expect(foundElements[3].getAttribute('id')).toEqual('sparkline-TEST');
    });

    it("should have prototype", function () {
        expect(StockQuotes.prototype instanceof SqRequestCreator).toEqual(true);
    });
});
