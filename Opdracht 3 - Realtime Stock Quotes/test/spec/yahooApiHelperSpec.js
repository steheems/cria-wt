/*global describe: true, beforeEach: true, it: true, YahooApiHelper: true, expect: true */
describe("YahooApiHelper", function () {
    'use strict';

    var helper;

    beforeEach(function () {
        helper = new YahooApiHelper();
    });

    it("should return the symbols", function () {
        helper.addSymbols(['TEST', '"ARRAY"']);
        helper.addSymbol('SINGLE');
        expect(helper.getSymbols()).toEqual(['"TEST"', '"ARRAY"', '"SINGLE"']);
    });

    it("should return the select fields", function () {
        helper.addSelectFields(['Test', '"Array"']);
        helper.addSelectField('Single');
        expect(helper.getSelectFields()).toEqual(['Test', '"Array"', 'Single']);
    });

    it("should return the correct url", function () {
        helper.addSymbol('TEST');
        helper.addSelectField('Symbol');
        expect(helper.getUrl()).toEqual('https://query.yahooapis.com/v1/public/yql?q=select%20Symbol%20from%20yahoo.finance.quotes%20where%20symbol%20in%0A(%22TEST%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=');
    });

    it("should return random data", function () {
        var randData, d = new Date();

        d.addHours(-6);

        helper.addSymbol('TEST');
        helper.addSelectField(['Symbol', 'Change', 'LastTradeTime', 'Test']);

        randData = helper.generateRandomData()[0];
        expect(randData.Symbol).toEqual('TEST');
        expect(randData.Change).toBeGreaterThan(-6);
        expect(randData.Change).toBeLessThan(6);
        expect(randData.LastTradeTime).toEqual(d.toString('h:mmtt'));
        expect(randData.Test).toEqual('temp');
    });

});
