/*global angular: true */
var GetManHelpers = GetManHelpers || {
    shortenString: function (string, length) {
        if(string.length > length) {
            return string.substr(0, length) + '...';
        } else {
            return string;
        }
    }
};

var getMan = angular.module('GetMan', ['ngRoute', 'ngResource']);