/*jslint browser: true */
/*global $: true, getMan: true, alert: true */
/**
 * Created by Erwin Heemsbergen on 10-1-2015.
 */
getMan.directive('login', ['$location', '$route', 'AuthService', function ($location, $route, AuthService) {
    'use strict';
    return {
        restrict: 'E',
        templateUrl: 'app/directives/loginDirective.html',
        replace: true,
        link: function ($scope) {
            $scope.currentUser = {};

            AuthService.loggedin().then(function (data) {
                $scope.currentUser = data.data;

                if ($scope.currentUser !== '0') {
                    $('.loggedout').hide();
                    $('.loggedin').show();
                }
            });

            $scope.login = function (user) {
                AuthService.login(user).then(function (curUser) {
                    $scope.currentUser = curUser.data;
                    $scope.hideLogin();
                    $scope.hideRegister();
                    $('.loggedout').hide();
                    $('.loggedin').show();
                    $route.reload();
                });
            };

            $scope.register = function (user) {
                AuthService.register(user).then(function () {
                    $scope.registerData = {};
                    $scope.hideRegister();
                    alert('You have been registered');
                });
            };

            $scope.logout = function () {
                AuthService.logout().then(function () {
                    $('.loggedin').hide();
                    $('.loggedout').show();
                    $location.path('/');
                });
            };

            $scope.clickLogin = function () {
                if ($('#login-popup').is(':hidden')) {
                    $scope.showLogin();
                } else {
                    $scope.hideLogin();
                }
            };

            $scope.showLogin = function () {
                $('#login-popup').show();
            };

            $scope.hideLogin = function () {
                $('#login-popup').hide();
            };

            $scope.clickRegister = function () {
                if ($('#register-popup').is(':hidden')) {
                    $scope.showRegister();
                } else {
                    $scope.hideRegister();
                }
            };

            $scope.showRegister = function () {
                $('#register-popup').show();
            };

            $scope.hideRegister = function () {
                $('#register-popup').hide();
            };

            $(document).mouseup(function (e) {
                var loginContainer = $('#login-popup'),
                    registerContainer = $('#register-popup');

                if (!loginContainer.is(e.target) && loginContainer.has(e.target).length === 0) {
                    loginContainer.hide();
                }

                if (!registerContainer.is(e.target) && registerContainer.has(e.target).length === 0) {
                    registerContainer.hide();
                }
            });
        }
    };
}]);