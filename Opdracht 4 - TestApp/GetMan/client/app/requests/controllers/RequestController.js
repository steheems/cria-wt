/*jslint nomen: true */
/*global getMan: true, console: true */
getMan.controller('RequestController', ['$scope', '$routeParams', '$location', 'RequestsService', function ($scope, $routeParams, $location, RequestsService) {
    'use strict';
    if ($routeParams.id !== undefined) {
        $scope.request = RequestsService.requests.get({id: $routeParams.id});
    } else {
        $scope.request = {
            doc: {}
        };
    }

    $scope.delete = function () {
        console.log('Deleting request');
        RequestsService.requests.delete({id: $routeParams.id});
        $location.path('/requests');
    };

    $scope.test = 0;

    $scope.save = function () {
        if ($scope.request.doc.url !== undefined && $scope.request.doc.method !== undefined) {
            if ($scope.request.doc && $scope.request.doc._id !== undefined) {
                console.log('Updating existing request');
                RequestsService.requests.update({id: $scope.request.doc._id}, $scope.request.doc);
                $location.path('/requests');
            } else {
                console.log('Saving new request');
                RequestsService.requests.save({}, $scope.request.doc);
                $location.path('/requests');
            }
        }
    };
}]);