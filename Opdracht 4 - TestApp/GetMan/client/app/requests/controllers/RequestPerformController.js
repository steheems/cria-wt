/*global getMan:true */
getMan.controller('RequestPerformController', ['$scope', '$routeParams', 'RequestsService', function ($scope, $routeParams, RequestsService) {
    'use strict';

    RequestsService.requests.get({
        id: $routeParams.id,
        perform: 'perform'
    }, function (data) {
        $scope.request = data;
    });
}]);