/*global getMan: true, GetManHelpers: true */
getMan.controller('RequestsController', ['$scope', '$location', 'RequestsService', function ($scope, $location, RequestsService) {
    'use strict';
    RequestsService.requests.get().$promise.then(function (data) {
        $scope.requests = data;
    });


    $scope.newRequest = function () {
        $location.path('/requests-add');
    };

    $scope.shortenString = GetManHelpers.shortenString;
}]);