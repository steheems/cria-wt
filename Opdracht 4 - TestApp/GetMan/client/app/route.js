/*global getMan: true */
getMan.config(['$routeProvider', function ($routeProvider) {
    'use strict';

    // Requests

    $routeProvider.when('/requests', {
        templateUrl: 'app/requests/partials/request-list.html',
        controller: 'RequestsController'
    });

    $routeProvider.when('/requests-add', {
        templateUrl: 'app/requests/partials/request-add.html',
        controller: 'RequestController'
    });

    $routeProvider.when('/requests/:id', {
        templateUrl: 'app/requests/partials/request-detail.html',
        controller: 'RequestController'
    });

    $routeProvider.when('/requests/:id/perform', {
        templateUrl: 'app/requests/partials/request-perform.html',
        controller: 'RequestPerformController'
    });

    // Tests

    $routeProvider.when('/tests', {
        templateUrl: 'app/tests/partials/test-list.html',
        controller: 'TestsController'
    });

    $routeProvider.when('/tests-add', {
        templateUrl: 'app/tests/partials/test-add.html',
        controller: 'TestController'
    });

    $routeProvider.when('/tests/:id', {
        templateUrl: 'app/tests/partials/test-detail.html',
        controller: 'TestController'
    });

    $routeProvider.when('/tests/:id/perform', {
        templateUrl: 'app/tests/partials/test-perform.html',
        controller: 'TestPerformController'
    });

    // Scenarios

    $routeProvider.when('/scenarios', {
        templateUrl: 'app/scenarios/partials/scenario-list.html',
        controller: 'ScenariosController'
    });

    $routeProvider.when('/scenarios-add', {
        templateUrl: 'app/scenarios/partials/scenario-add.html',
        controller: 'ScenarioController'
    });

    $routeProvider.when('/scenarios/:id', {
        templateUrl: 'app/scenarios/partials/scenario-detail.html',
        controller: 'ScenarioController'
    });

    $routeProvider.when('/scenarios/:id/perform', {
        templateUrl: 'app/scenarios/partials/scenario-perform.html',
        controller: 'ScenarioPerformController'
    });

    // Users

    $routeProvider.when('/users', {
        templateUrl: 'app/users/partials/user-list.html',
        controller: 'UsersController'
    });

    $routeProvider.when('/user-add', {
        templateUrl: 'app/users/partials/user-add.html',
        controller: 'UserController'
    });

    $routeProvider.when('/users/:id', {
        templateUrl: 'app/users/partials/user-detail.html',
        controller: 'UserController'
    });
}]);