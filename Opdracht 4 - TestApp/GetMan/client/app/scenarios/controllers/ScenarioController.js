/*jslint nomen: true */
/*global getMan: true, console: true */
getMan.controller('ScenarioController', ['$scope', '$routeParams', '$location', 'TestsService', 'ScenariosService', function ($scope, $routeParams, $location, TestsService, ScenariosService) {
    'use strict';
    $scope.tests = TestsService.tests.get();

    if ($routeParams.id !== undefined) {
        ScenariosService.scenarios.get({id: $routeParams.id}).$promise.then(function (data) {
            $scope.scenario = data.doc;
        });
    } else {
        $scope.scenario = {};
    }

    $scope.delete = function () {
        console.log('Deleting scenario');
        ScenariosService.scenarios.delete({id: $routeParams.id});
        $location.path('/scenarios');
    };

    $scope.save = function () {
        if ($scope.scenario.request !== undefined) {
            if ($scope.scenario._id !== undefined) {
                console.log('Updating existing scenario');
                ScenariosService.scenarios.update({id: $scope.scenario._id}, $scope.scenario);
                $location.path('/scenarios');
            } else {
                console.log('Saving new scenario');
                ScenariosService.scenarios.save({}, $scope.scenario).$promise.then(function (data) {
                    console.log(data);
                    $location.path('/scenarios');
                });
            }
        }
    };
}]);