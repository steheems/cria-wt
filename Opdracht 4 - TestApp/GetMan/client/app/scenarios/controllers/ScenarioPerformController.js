/*global getMan: true */
getMan.controller('ScenarioPerformController', ['$scope', '$routeParams', 'ScenariosService', function ($scope, $routeParams, ScenariosService) {
    'use strict';

    ScenariosService.scenarios.get({
        id: $routeParams.id,
        perform: 'perform'
    }, function (data) {
        $scope.scenario = data;
    });
}]);