/*global getMan: true */
getMan.controller('ScenariosController', ['$scope', '$location', 'ScenariosService', function ($scope, $location, ScenariosService) {
    'use strict';
    $scope.scenarios = ScenariosService.scenarios.get();

    $scope.newScenario = function () {
        $location.path('/scenarios-add');
    };
    
    $scope.shortenString = GetManHelpers.shortenString;
}]);