/*global getMan: true */
getMan.factory('TestsService', ['$resource',
    function ($resource) {
        'use strict';

        var actions = {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'query': {method: 'GET', isArray: true},
            'delete': {method: 'DELETE'}
        }, db = {};

        db.tests = $resource('/tests/:id/:perform', {}, actions);
        return db;
    }]);

getMan.factory('RequestsService', ['$resource',
    function ($resource) {
        'use strict';

        var actions = {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'query': {method: 'GET', isArray: true},
            'delete': {method: 'DELETE'}
        }, db = {};

        db.requests = $resource('/requests/:id/:perform', {}, actions);
        return db;
    }]);

getMan.factory('ScenariosService', ['$resource',
    function ($resource) {
        'use strict';

        var actions = {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'query': {method: 'GET', isArray: true},
            'delete': {method: 'DELETE'}
        }, db = {};

        db.scenarios = $resource('/scenarios/:id/:perform', {}, actions);
        return db;
    }]);

getMan.factory('AuthService', ['$http',
    function ($http) {
        'use strict';
        var authService = {};

        authService.login = function (user) {
            return $http.post('/login', {email: user.email, password: user.password}).then(function (res) {
                return res;
            });
        };

        authService.register = function (user) {
            return $http.post('/users', {name: user.name, email: user.email, password: user.password}).then(function (res) {
                return res;
            });
        };

        authService.logout = function () {
            return $http.get('/logout');
        };

        authService.loggedin = function () {
            return $http.get('/login').then(function (res) {
                return res;
            });
        };

        return authService;
    }]);

getMan.factory('UsersService', ['$resource',
    function ($resource) {
        'use strict';

        var actions = {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'query': {method: 'GET', isArray: true},
            'delete': {method: 'DELETE'}
        }, db = {};

        db.users = $resource('/users/:id', {}, actions);
        return db;
    }]);