/*jslint nomen: true */
/*global getMan: true, console: true */
getMan.controller('TestController', ['$scope', '$routeParams', '$location', 'TestsService', 'RequestsService', function ($scope, $routeParams, $location, TestsService, RequestsService) {
    'use strict';
    $scope.requests = RequestsService.requests.get();

    if ($routeParams.id !== undefined) {
        TestsService.tests.get({id: $routeParams.id}).$promise.then(function (data) {
            $scope.test = data.doc;
        });
    } else {
        $scope.test = {};
    }

    $scope.delete = function () {
        console.log('Deleting test');
        TestsService.tests.delete({id: $routeParams.id});
        $location.path('/tests');
    };

    $scope.save = function () {
        if ($scope.test.request !== undefined) {
            if ($scope.test._id !== undefined) {
                console.log('Updating existing test');
                TestsService.tests.update({id: $scope.test._id}, $scope.test);
                $location.path('/tests');
            } else {
                console.log('Saving new test');
                TestsService.tests.save({}, $scope.test).$promise.then(function (data) {
                    console.log(data);
                    $location.path('/tests');
                });
            }
        }
    };
}]);