/*global getMan: true */
getMan.controller('TestPerformController', ['$scope', '$routeParams', 'TestsService', function ($scope, $routeParams, TestsService) {
    'use strict';

    TestsService.tests.get({
        id: $routeParams.id,
        perform: 'perform'
    }, function (data) {
        $scope.test = data;
    });
}]);