/*global getMan: true */
getMan.controller('TestsController', ['$scope', '$location', 'TestsService', function ($scope, $location, TestsService) {
    'use strict';
    $scope.tests = TestsService.tests.get();

    $scope.newTest = function () {
        $location.path('/tests-add');
    };
    
    $scope.shortenString = GetManHelpers.shortenString;
}]);