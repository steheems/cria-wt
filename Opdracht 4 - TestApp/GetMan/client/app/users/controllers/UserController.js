/*jslint nomen: true */
/*global getMan: true, console: true */
getMan.controller('UserController', ['$scope', '$routeParams', '$location', 'UsersService', function ($scope, $routeParams, $location, UsersService) {
    'use strict';

    if ($routeParams.id !== undefined) {
        UsersService.users.get({id: $routeParams.id}).$promise.then(function (data) {
            $scope.user = data.doc;
        });
    } else {
        $scope.user = {};
    }

    $scope.delete = function () {
        console.log('Deleting user');
        UsersService.users.delete({id: $routeParams.id});
        $location.path('/users');
    };

    $scope.save = function () {
        if ($scope.user.email !== undefined && $scope.user.password !== undefined) {
            if ($scope.user._id !== undefined) {
                console.log('Updating existing user');
                UsersService.users.update({id: $scope.user._id}, $scope.user);
                $location.path('/users');
            } else {
                console.log('Saving new user');
                UsersService.users.save({}, $scope.user).$promise.then(function (data) {
                    console.log(data);
                    $location.path('/users');
                });
            }
        }
    };
}]);