/*jslint nomen: true */
/*global getMan: true, $: true, confirm: true */
getMan.controller('UsersController', ['$scope', '$location', 'UsersService', function ($scope, $location, UsersService) {
    'use strict';
    $scope.getUserList = function (callback) {
        UsersService.users.get().$promise.then(callback);
    };

    $scope.getUserList(function (data) {
        $('.authorized').show();
        $('.unauthorized').hide();
        $scope.users = data;
    });

    $scope.newUser = function () {
        $location.path('/user-add');
    };

    $scope.deleteUser = function (user) {
        if (confirm('Are you sure you want to remove the user with email address: ' + user.email + '?')) {
            UsersService.users.delete({id: user._id}).$promise.then(function () {
                $scope.getUserList(function (data) {
                    $scope.users = data;
                });
            });
        }
    };
}]);