/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var mongoose = require('mongoose'),
    Group = mongoose.model('Group');

exports.create = function (req, res) {
    'use strict';
    console.log('CREATE group');

    var doc = new Group(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date(), filename: __filename},
            doc: doc,
            err: err
        };

        return res.send(retObj);
    });
};

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE group');

    if (req.params.name) {
        Group.findOne({name: req.params.name}).populate('users').exec(function (err, doc) {
            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                err = 'Could not find item with this query';
            }
            var retObj = {
                meta: {"action": "retrieve one", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
    } else {
        Group.find().populate('users').exec(function (err, docs) {
            var retObj = {
                meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
                doc: docs,
                err: err
            };

            return res.send(retObj);
        });
    }
};