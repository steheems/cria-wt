/*jslint plusplus: true, nomen: true, es5: true */
/*global exports: true, require: true, console: true, __filename: true */
var mongoose = require('mongoose'),
    Request = mongoose.model('Request');

exports.create = function (req, res) {
    'use strict';
    console.log('CREATE request');

    var newRequest = {
        method: req.body.method || '',
        url: req.body.url || '',
        port: req.body.port || 80,
        path: req.body.path || '',
        sendData: req.body.sendData || ''
    }, doc = new Request(newRequest);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date(), filename: __filename},
            doc: doc,
            err: err
        };

        return res.send(retObj);
    });
};

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE request');

    if (req.params.id) {
        Request.findById(req.params.id, function (err, doc) {
            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                err = 'Could not find item with this query';
            }
            var retObj = {
                meta: {"action": "retrieve one", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
    } else {
        Request.find({}, function (err, docs) {
            var retObj = {
                meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
                doc: docs,
                err: err
            };

            return res.send(retObj);
        });
    }
};

exports.update = function (req, res) {
    'use strict';
    console.log('Updating request\n', req.params.id);

    var conditions = {_id: req.params.id},
        update = {
            method: req.body.method || '',
            url: req.body.url || '',
            port: req.body.port || 80,
            path: req.body.path || '',
            sendData: req.body.sendData || ''
        },
        options = {multi: false},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    Request.findOneAndUpdate(conditions, update, options, callback);
};

exports.delete = function (req, res) {
    'use strict';
    console.log('Deleting request\n', req.params.id);

    var conditions = {_id: req.params.id},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    Request.remove(conditions, callback);
};

exports.perform = function (req, res) {
    'use strict';
    console.log('PERFORM request');

    Request.findById(req.params.id, function (err, doc) {
        if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
            err = 'Could not find item with this query';
        }

        doc.perform(function (result) {
            var retObj = {
                meta: {"action": "perform request", "timestamp": new Date(), filename: __filename},
                result: result,
                err: err
            };

            res.send(retObj);
        });
    });
};