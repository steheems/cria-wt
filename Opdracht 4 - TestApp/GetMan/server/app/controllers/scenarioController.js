/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var mongoose = require('mongoose'),
    Scenario = mongoose.model('Scenario');

exports.create = function (req, res) {
    'use strict';
    console.log('CREATE request');

    var doc = new Scenario(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date(), filename: __filename},
            doc: doc,
            err: err
        };

        return res.send(retObj);
    });
};

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE group');

    if (req.params.id) {
        Scenario.findOne({_id: req.params.id}).exec(function (err, doc) {
            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                err = 'Could not find item with this query';
            }
            var retObj = {
                meta: {"action": "retrieve one", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
    } else {
        Scenario.find().exec(function (err, docs) {
            var retObj = {
                meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
                doc: docs,
                err: err
            };

            return res.send(retObj);
        });
    }
};

exports.perform = function (req, res) {
    'use strict';
    console.log('PERFORM scenario');

    Scenario.findById(req.params.id, function (err, doc) {
        if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
            err = 'Could not find item with this query';
        }

        doc.perform(function (result) {
            var retObj = {
                meta: {"action": "perform scenario", "timestamp": new Date(), filename: __filename},
                result: result,
                err: err
            };

            res.send(retObj);
        });
    });
};