/*jslint plusplus: true, nomen: true, es5: true  */
/*global exports: true, require: true, console: true, __filename: true */
var mongoose = require('mongoose'),
    Test = mongoose.model('Test');

exports.create = function (req, res) {
    'use strict';
    console.log('CREATE test');

    var doc = new Test(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date(), filename: __filename},
            doc: doc,
            err: err
        };

        return res.send(retObj);
    });
};

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE test');

    if (req.params.id) {
        Test.findById(req.params.id, function (err, doc) {
            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                err = 'Could not find item with this query';
            }
            var retObj = {
                meta: {"action": "retrieve one", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
    } else {
        Test.find({}, function (err, docs) {
            var retObj = {
                meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
                doc: docs,
                err: err
            };

            return res.send(retObj);
        });
    }
};

exports.update = function (req, res) {
    'use strict';
    console.log('Updating test\n', req.params.id);

    var conditions = {_id: req.params.id},
        update = {
            expectedResult: req.body.expectedResult || '',
            request: req.body.request || ''
        },
        options = {multi: false},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    Test.findOneAndUpdate(conditions, update, options, callback);
};

exports.delete = function (req, res) {
    'use strict';
    console.log('Deleting test\n', req.params.id);

    var conditions = {_id: req.params.id},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    Test.remove(conditions, callback);
};

exports.perform = function (req, res) {
    'use strict';
    console.log('PERFORM test');

    Test.findById(req.params.id, function (err, doc) {
        if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
            err = 'Could not find item with this query';
        }

        doc.perform(function (result) {
            var retObj = {
                meta: {"action": "perform test", "timestamp": new Date(), filename: __filename},
                result: result,
                err: err
            };

            res.send(retObj);
        });
    });
};