/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Group = mongoose.model('Group');

exports.create = function (req, res) {
    'use strict';
    console.log('CREATE user');

    var doc = new User(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date(), filename: __filename},
            doc: doc,
            err: err
        };

        if (doc.group !== undefined && doc.group !== null) {
            Group.findById(doc.group, function (err, doc) {
                if (err) {
                    console.log(err);
                }

                doc.users.push(doc);
                doc.save();
            });
        }

        return res.send(retObj);
    });
};

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE user');

    if (req.params.id) {
        User.findById(req.params.id).populate('group').exec(function (err, doc) {
            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                err = 'Could not find item with this query';
            }
            var retObj = {
                meta: {"action": "retrieve one", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
    } else {
        User.find().populate('group').exec(function (err, docs) {
            var retObj = {
                meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
                doc: docs,
                err: err
            };

            return res.send(retObj);
        });
    }
};

exports.update = function (req, res) {
    'use strict';
    console.log('Updating user\n', req.params.id);

    var conditions = {_id: req.params.id},
        callback = function (err, doc) {
            if(err === null) {
                doc.name = req.body.name || '';
                doc.email = req.body.email || '';
                
                if (doc.password !== req.body.password) {
                    doc.password = req.body.password || '';
                    doc.encrypted = false;
                } else{
                    doc.encrypted = true;
                }

                doc.save();
            }
            
            var retObj = {
                meta: {"action": "update", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        };

    //User.findOneAndUpdate(conditions, update, options, callback);
    User.findOne(conditions, callback);
};

exports.delete = function (req, res) {
    'use strict';
    console.log('DELETE user\n', req.params.id);

    var conditions = {_id: req.params.id},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    User.remove(conditions, callback);
};