/*jslint plusplus: true */
/*global exports: true, require: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schemaName = new Schema({
    name: {type: String, required: true, unique: true},
    users: [{type: Schema.ObjectId, ref: "User"}]
});

schemaName.path('name').validate(function (val) {
    'use strict';
    return (val !== undefined && val !== null);
}, 'Name is empty');

var modelName = "Group";
var collectionName = "groups";
mongoose.model(modelName, schemaName, collectionName);