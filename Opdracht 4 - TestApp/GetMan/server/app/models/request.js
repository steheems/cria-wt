/*jslint plusplus: true */
/*global exports: true, require: true, console: true, XMLHttpRequest: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var http = require('http');

var schemaName = new Schema({
    method: {type: String, required: true},
    url: {type: String, required: true},
    path: {type: String, required: false},
    port: {type: Number, required: false, default: 80},
    sendData: {type: String, required: false}
});

schemaName.path('method').validate(function (val) {
    'use strict';
    return (val !== undefined && val !== null && (val.toUpperCase() === 'GET' || val.toUpperCase() === 'POST' || val.toUpperCase() === 'PUT' || val.toUpperCase() === 'DELETE'));
}, 'Request method is invalid');

schemaName.path('url').validate(function (val) {
    'use strict';
    return (val !== undefined && val !== null);
}, 'url is empty');

schemaName.methods.perform = function (callback) {
    'use strict';
    var options = {
        hostname: this.url,
        method: this.method,
        port: this.port || 80,
        path: this.path || '',
        agent: false
    };

    if (this.method.toUpperCase() !== 'GET') {
        options.headers = {
            'Content-Type': 'application/json',
            'Content-Length': this.sendData.length
        };
    }

    var req = http.request(options, function (res) {
        var data = '';
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));

        res.setEncoding('utf8');

        res.on('data', function (chunk) {
            data += chunk;
        });

        res.on('end', function () {
            callback(data);
        });
    });

    req.on('error', function (e) {
        console.log('problem with request: ' + e.message);
    });

    if (this.sendData) {
        req.write(this.sendData);
    }

    req.end();
};

var modelName = "Request";
var collectionName = "requests";
mongoose.model(modelName, schemaName, collectionName);