/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Test = mongoose.model('Test');

var schemaName = new Schema({
    name: {type: String, required: true, unique: true},
    tests: [{type: Schema.ObjectId, ref: "Test"}]
});

schemaName.path('name').validate(function (val) {
    'use strict';
    return (val !== undefined && val !== null);
}, 'Name is empty');

schemaName.methods.perform = function (callback) {
    'use strict';
    var resultsObject = {}, cb, findTest, testIndex = 0, _this = this;

    cb = function (resultBool, resultRaw, testId) {
        resultsObject[testId] = {
            bool: resultBool,
            raw: resultRaw
        };

        testIndex++;

        if (_this.tests.length > testIndex) {
            findTest(testIndex, cb);
        } else {
            callback(resultsObject);
        }
    };

    findTest = function (testIndex, testCallback) {
        Test.findOne({_id: _this.tests[testIndex]}, function (err, doc) {
            if (doc === null && err === null) {
                err = 'Could not find item with this query';
            }

            if (err) {
                console.log(err);
                testCallback(err);
            } else {
                doc.perform(testCallback);
            }
        });
    };

    findTest(testIndex, cb);
};

var modelName = "Scenario";
var collectionName = "scenarios";
mongoose.model(modelName, schemaName, collectionName);