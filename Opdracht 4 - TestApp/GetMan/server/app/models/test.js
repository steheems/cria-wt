/*jslint plusplus: true nomen: true */
/*global exports: true, require: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Request = mongoose.model('Request');

var schemaName = new Schema({
    expectedResult: {type: String, default: ''},
    request: {type: Schema.ObjectId, ref: "Request", required: true}
});

schemaName.path('expectedResult').validate(function (val) {
    'use strict';
    return (val !== undefined && val !== null);
}, 'expectedResult cannot be empty');

schemaName.methods.perform = function (callback) {
    'use strict';
    var expected = this.expectedResult,
        _this = this,
        rqCb = function (response) {
            callback(response === expected, response, _this._id);
        };

    Request.findById(this.request, function (err, doc) {
        if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
            err = 'Could not find item with this query';
        } else {
            doc.perform(rqCb);
        }
    });
};

var modelName = "Test";
var collectionName = "tests";
mongoose.model(modelName, schemaName, collectionName);