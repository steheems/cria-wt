/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, Buffer: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto');

var schemaName = new Schema({
    name: {type: String},
    email: {type: String, required: true, unique: true},
    group: {type: Schema.ObjectId, ref: "Group"},
    password: {
        type: String,
        default: ''
    },
    salt: {
        type: String
    },
    encrypted: {type: Boolean, default: false}
});

schemaName.path('email').validate(function (val) {
    'use strict';
    var emailRegex = /^[A-Z0-9_%+\-]+@[A-Z0-9\-]+\.[A-Z]{2,6}/ig;
    return (val !== undefined && val !== null && emailRegex.test(val));
}, 'Invalid email address');

schemaName.path('password').validate(function (val) {
    'use strict';
    return (val.length >= 6);
}, 'Password is too short');

/**
 * Hook a pre save method to hash the password
 */
schemaName.pre('save', function (next) {
    'use strict';
    if (this.password && this.password.length >= 6 && !this.encrypted) {
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
        this.encrypted = true;
    }

    next();
});

/**
 * Create instance method for hashing a password
 */
schemaName.methods.hashPassword = function (password) {
    'use strict';
    if (this.salt && password) {
        return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
    }

    return password;
};

/**
 * Create instance method for authenticating user
 */
schemaName.methods.authenticate = function (password) {
    'use strict';
    return this.password === this.hashPassword(password);
};

/**
 * Find possible not used username
 */
schemaName.statics.findUniqueUsername = function (username, suffix, callback) {
    'use strict';
    var _this = this,
        possibleUsername = username + (suffix || '');

    _this.findOne({
        username: possibleUsername
    }, function (err, user) {
        if (!err) {
            if (!user) {
                callback(possibleUsername);
            } else {
                return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
            }
        } else {
            callback(null);
        }
    });
};

var modelName = "User";
var collectionName = "users";
mongoose.model(modelName, schemaName, collectionName);