/*jslint nomen: true */
/*global require: true, module: true */
var passport = require('passport'),
    User = require('mongoose').model('User'),
    path = require('path');

module.exports = function () {
    'use strict';
    // Serialize sessions
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // Deserialize sessions
    passport.deserializeUser(function (id, done) {
        User.findOne({
            _id: id
        }, '-salt -password', function (err, user) {
            done(err, user);
        });
    });

    // Initialize strategies
    require('./config/localStrategy')();
};