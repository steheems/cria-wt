/*global module:true, require: true */
/**
 * Authorization route, contains actions concerning logging in and out
 * Sending a GET request to /login returns the currently logged in user,
 *  if no-one is logged in returns a 0
 * Sending a POST request to /login with a JSON object containing
 *  email and password fields will try to log in with that data
 * Sending both a GET or a POST request to /logout will log out the currently
 * logged in user
 *
 * @param app
 */
module.exports = function (app) {
    'use strict';
    var passport = require('passport');

    // route to know whether or not somone is logged in
    app.get('/login', function (req, res) {
        res.send(req.isAuthenticated() ? req.user : '0');
    });

    // route to log in
    app.post('/login', passport.authenticate('local'), function (req, res) {
        req.user.salt = '';
        req.user.password = '';
        res.send(req.user);
    });

    // route to log out
    app.get('/logout', function (req, res) {
        req.logOut();
        res.sendStatus(200);
    });

    // route to log out
    app.post('/logout', function (req, res) {
        req.logOut();
        res.sendStatus(200);
    });
};