/*global module:true, require: true */
/**
 * Group route, currently serves no purpose
 *
 * @param app
 */
module.exports = function (app) {
    'use strict';
    var controller = require('../app/controllers/groupController.js');

    // CREATE
    app.post('/groups', controller.create);

    // RETRIEVE ALL
    app.get('/groups', controller.retrieve);

    // RETRIEVE ONE
    app.get('/groups/:name', controller.retrieve);

    //UPDATE

    //DELETE

};