/*global module:true, require: true */
/**
 * Request Route, handles all requests to /requests
 * CRUD operations are all available
 * requests have a 'perform' action which performs the request
 *
 * @param app
 */
module.exports = function (app) {
    'use strict';
    var controller = require('../app/controllers/requestController.js');

    // CREATE
    app.post('/requests', controller.create);

    // RETRIEVE ALL
    app.get('/requests', controller.retrieve);

    // RETRIEVE ONE
    app.get('/requests/:id', controller.retrieve);

    // PERFORM
    app.get('/requests/:id/perform', controller.perform);

    //UPDATE
    app.put('/requests/:id', controller.update);

    //DELETE
    app.delete('/requests/:id', controller.delete);

};