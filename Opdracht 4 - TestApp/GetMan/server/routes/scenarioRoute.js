/*global module:true, require: true */
/**
 * Scenario Route, handles all requests to /scenarios
 * Currently only Create and Retrieve all operations are available, perform
 * action is being developed 
 * scenarios have a 'perform' action which performs the tests which are linked
 * to this scenario
 *
 * @param app
 */
module.exports = function (app) {
    'use strict';
    var controller = require('../app/controllers/scenarioController.js');

    // CREATE
    app.post('/scenarios', controller.create);

    // RETRIEVE ALL
    app.get('/scenarios', controller.retrieve);

    // RETRIEVE ONE
    app.get('/scenarios/:id', controller.retrieve);

    // PERFORM
    app.get('/scenarios/:id/perform', controller.perform);

    // UPDATE

    // DELETE
};