/*global module:true, require: true */
/**
 * Test Route, handles all requests to /tests
 * CRUD operations are all available
 * tests have a 'perform' action which performs the test, performing the
 * requests linked to this test and comparing the outcome to the expected
 * result of this test.
 *
 * @param app
 */
module.exports = function (app) {
    'use strict';
    var controller = require('../app/controllers/testController.js');

    // CREATE
    app.post('/tests', controller.create);

    // RETRIEVE ALL
    app.get('/tests', controller.retrieve);

    // RETRIEVE ONE
    app.get('/tests/:id', controller.retrieve);

    // PERFORM
    app.get('/tests/:id/perform', controller.perform);

    //UPDATE
    app.put('/tests/:id', controller.update);

    //DELETE
    app.delete('/tests/:id', controller.delete);

};