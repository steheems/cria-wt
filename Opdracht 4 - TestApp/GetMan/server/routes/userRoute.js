/*global module:true, require: true */
var isAuthenticated = function (req, res, next) {
    'use strict';
    if (req.isAuthenticated()) {
        return next();
    }
    res.sendStatus(401);
};

/**
 * User Route, handles all requests to /users
 * CRUD operations (apart from creation) cannot be performed without being logged in
 *
 * @param app
 */
module.exports = function (app) {
    'use strict';
    var controller = require('../app/controllers/userController.js');

    // CREATE
    app.post('/users', controller.create);

    // RETRIEVE ALL
    app.get('/users', isAuthenticated, controller.retrieve);

    // RETRIEVE ONE
    app.get('/users/:id', isAuthenticated, controller.retrieve);

    //UPDATE
    app.put('/users/:id', isAuthenticated, controller.update);
    
    //DELETE
    app.delete('/users/:id', isAuthenticated, controller.delete);
};